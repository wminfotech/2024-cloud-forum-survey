FROM docker.io/library/python:3.12 AS build

WORKDIR /app
COPY . /app/

RUN pip install --no-cache-dir -r requirements.txt \
    && mkdocs build

FROM docker.io/library/nginx:1

COPY --from=build /app/site /usr/share/nginx/html