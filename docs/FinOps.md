FinOps
======

We asked respondents about how they support FinOps at their institution. We use the definition of FinOps developed by the [FinOps Foundation](https://www.finops.org/): a cloud financial management discipline and cultural practice that enables engineering, finance, technology, and business teams to collaborate on data-driven spending decisions to maximize the value of cloud.

We asked users where they see themselves along the FinOps journey. Ordered least mature to most mature, the options were:

1. Awareness - Understand the importance of cloud cost management but taken no meaningful action (e.g. We review our cloud provider invoices to some extent and feel they are generally accurate or We are still figuring out where to start)
2. Visibility - Beginning the journey of managing cloud costs (e.g. We have established basic cost visibility into our cloud operations)
3. Activity - Building a foundation for financial cloud cost management (e.g. We have implemented some cost optimization practices such as reservations and savings plans).
4. Engagement - Applying advanced cost optimization strategies and financial reporting practices with collaboration among cloud cost stakeholders (engineering, finance, leadership, customers) (e.g. We are using automation for various cost management activities (rightsizing, workload management, reservation allocation) and negotiating discounts with providers based on spend)

![FinOps Journey](https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=820182431&format=image)

We asked how many employees work on FinOps at each institution.

![FinOps Employees](https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=1629886615&format=image)

We asked which select [capabilities](https://www.finops.org/framework/capabilities/) each respondent's organization engages in, and how frequently.

<iframe frameBorder="0" width="650" height="400" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=1014102780&format=interactive"></iframe>