Technologies and Tools
======================

## What technologies are institutions adopting?

<iframe frameBorder="0" width="650" height="400" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=393961796&format=interactive"></iframe>

## Infrastructure as Code

We asked users how much of their infrastructure is managed as code (IaC), regardless of tool.

![Infrastructure as Code Use](https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=1788590725&format=image)

[Terraform](https://www.terraform.io/)/[OpenTofu](https://opentofu.org/) is the most popular Infrastructure as Code tool, used by 74% of respondents.

<iframe frameBorder="0" width="650" height="400" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=577039293&format=interactive"></iframe>


Traditionally IaC is used in cloud environments. We also asked whether or not anything on-premise is managed as code.

![Infrastructure as Code On-Prem](https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=698751475&format=image)