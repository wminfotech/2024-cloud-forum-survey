2024 Higher Education State of Cloud Survey
===========================================

The data on this site is compiled from 59 survey responses representing 55 institutions
in preparation for the [Higher Education Cloud Forum](https://internet2.edu/cloud-forum/) and also presented to the [EDUCAUSE Cloud Computing Community Group](https://www.educause.edu/community/cloud-computing-community-group).

Users were asked to respond for their user of Infrastructure and platform as a service purchases only.

Data isn't de-duplicated when there are multiple responses from a single institution because
each response is from the perspective of the area (e.g. central IT, department).

## Makeup of Responses

![Carnegie Classifications](https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=1081629009&format=image)


![Organization Location](https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=633674400&format=image)

## Cloud Providers Used

![Average Cloud Spend by Response](https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=770484284&format=image)

| Cloud Service Provider | Any Presence | Maximum |
|------------------------|--------------|---------|
| AWS                    | 56 (95%)     | 100%    |
| Azure                  | 52 (88%)     | 99%     |
| Google                 | 39 (65%)     | 100%    |
| Oracle                 | 9 (15%)      | 20%     |
| IBM                    | 1 (2%)       | 1%      |

- 34 respondents (57%) have a single primary cloud provider (≥ 70% of their spend is with a single provider):

23 respondents are primarily in AWS, 7 in Azure, 3 in Google

- 17 respondents (29%) are multi-cloud and have no single primary provider (no more than 60% with any one provider)
- 12 respondents (20%) are multi-cloud and spend 10% or more in each of the 3 major cloud providers
- 53 respondents (90%) have some presence in multiple clouds

We asked respondents to tell us if they're using an ITAR compliant cloud (e.g. Amazon's GovCloud) for any restricted data

![ITAR Compliant Cloud](https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=1140246506&format=image)

## Cloud Strategies

We asked respondents for general summaries of their cloud strategy and how well defined it is.

![Cloud Strategy](https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=1165485041&format=image)

![How Well Cloud Definition is Defined](https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=408483061&format=image)

## Why move to the cloud?

<iframe frameBorder="0" width="650" height="400" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=1512699218&format=interactive"></iframe>

## What prevents cloud adoption?

<iframe frameBorder="0" width="650" height="400" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=61879434&amp;format=interactive"></iframe>

