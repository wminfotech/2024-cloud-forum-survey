Exiting the Cloud
=================

We found that roughly 48% of respondents had moved at least one workload from the cloud

![Cloud Exit](https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=59957518&format=image)

the reasons for moving also varied:

<iframe frameBorder="0" width="650" height="400" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=1939146153&format=interactive"></iframe>

- 6 respondents cited technical reasons as the only reason for moving workloads out of the cloud.

- Of the 15 respondents that cited cost, 9 of them cited cost alone. 5 other respondents cited
both cost and technical reasons for moving workloads.

- The one respondent citing management reasons also cited technical reasons as why they moved a workload out of the cloud.
