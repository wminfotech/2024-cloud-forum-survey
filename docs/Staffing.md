Staffing
========

We asked how teams are structured (i.e. does cloud fit in a distinct team, part of a larger team, etc.) what cloud related services that team offers, and how frequently they provide the service.

![Team Responsibilities](https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=2102494917&format=image)

<iframe frameBorder="0" width="650" height="400" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=638118130&format=interactive"></iframe>

We found that teams sizes range from 1 to 200 people, but average just under 13. We asked about team size, not those dedicated to cloud.

<iframe frameBorder="0" width="650" height="400" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=1316960131&format=interactive"></iframe>

We also asked about full time employee (FTE) count knowing it's often a different than head count. FTE also ranged from 1-200, but averaged just under 12. Excluding the 200 FTE outlier, the average dropped to 9.5.

<iframe frameBorder="0" width="650" height="400" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=232025900&format=interactive"></iframe>

## Cloud Enablement

We specifically asked if a respondent has a cloud enablement team.

![Cloud Enablement Team](https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=695119576&format=image)