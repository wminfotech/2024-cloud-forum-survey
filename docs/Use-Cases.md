Cloud Use Cases
===============

Respondents were asked which areas they support at their institution (e.g. administrative, instructional, and research) as well as how those areas are funded.

<iframe frameBorder="0" width="650" height="400" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=2081577584&format=interactive"></iframe>

- Instructional use cases were never supported alone
- 51 respondents (86%) support more than one use case
- 5 respondents support only administrative uses
- 3 respondents support only research uses

## Funding Sources

<iframe frameBorder="0" width="650" height="400" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ-JMxJg8lum5f4TiehgFPmZvDKqtkyJDE5RFyN1YJRZS8vVlROPezLs6r9Tsk-0hrSp09In_mkocW6/pubchart?oid=1480586608&format=interactive"></iframe>

### Research

67% of teams that support research are partially funded by cloud credits